# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return


# Function to inherit an updated SSH auth socket in a reattached tmux session
function update_ssh_auth_sock() {
    if [[ ! -z $TMUX && ! -z $SSH_CONNECTION ]]
    then
        SOCK=$(tmux show-environment | egrep ^SSH_AUTH_SOCK=);
        if [[ ! -z $SOCK ]]
        then
          export $SOCK
        fi
    fi
}

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Fetch hostname
HOST=$(hostname)

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# Handle gnome-terminals lack of TERM assignment
if [[ $COLORTERM == "gnome-terminal" ]]
then
  echo "Fixing gnome-terminal's TERM";
  export TERM=xterm-256color
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*color*) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

GIT_PS1_SHOWDIRTYSTATE=true

PSCOLOR=32m;
if [[ $HOST == *.api.kunder.linpro.no || $OSTYPE =~ "darwin" ]]; then
  . ~/dots/git-prompt.sh
  . ~/dots/git-completion.bash
fi

echo "Checking for env for os-$OSTYPE"
if [ -f ~/dots/env.os-$OSTYPE ]
then
    echo "Loading env for os-$OSTYPE"
    . ~/dots/env.os-$OSTYPE
fi

## Set prompt color
if [[ $HOST == *.api.kunder.linpro.no || $HOST == login-osl2 ]]; then
  PSCOLOR=31m;
elif [[ $HOST == shaun ]]; then
  PSCOLOR=33m;
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;'$PSCOLOR'\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w$(__git_ps1)\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# Set the prompt command to enable updating of SSH auth socket in tmux
# This should probably be set in tmux config instead.
export PROMPT_COMMAND=update_ssh_auth_sock;

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# Cybic's scripts
PATH="$HOME/dots/scripts:$HOME/bin:$PATH"

if [ -f ~/dots/dirb/dirb.sh ]
then
    . ~/dots/dirb/dirb.sh
fi

echo "Checking for env for $HOST"
if [ -f ~/dots/env.$HOST ]
then
    echo "Loading env for $HOST"
    . ~/dots/env.$HOST
fi

HOSTN=$(echo $HOST | sed 's/\..*//')
if [ "$HOST" != "$HOSTN" ]
then
  echo "Checking for env for $HOSTN"
  if [ -f ~/dots/env.$HOSTN ]
  then
    echo "Loading env for $HOSTN"
    . ~/dots/env.$HOSTN
  fi
fi
